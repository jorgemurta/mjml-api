const mjml2html = require('mjml');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

app.use(bodyParser.json());

app.post('/', (req, res) => {
  const { mjml } = req.body;
  const mjmlOptions = {
    minify: true,
    keepComments: false
  };
  res.send(mjml2html(mjml, mjmlOptions));
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

FROM node:10.16-alpine

WORKDIR /usr/src/app
EXPOSE 8080

COPY package*.json ./
RUN npm ci --only=production

COPY . .

CMD [ "node", "index.js" ]